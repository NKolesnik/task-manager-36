package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.IUserService;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ModelNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;
import ru.t1consulting.nkolesnik.tm.repository.UserRepository;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class UserServiceTest {

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_LOGIN_PREFIX = "TEST_USER_LOGIN";

    @NotNull
    private static final String USER_EMAIL_PREFIX = "TEST_USER_@EMAIL";

    @NotNull
    private static final String USER_PASSWORD_PREFIX = "TEST_USER_PASSWORD";

    @NotNull
    private static final String USER_PASSWORD_SECRET = "123654789";

    @NotNull
    private static final Integer USER_PASSWORD_ITERATION = 3;

    @NotNull
    private static final String USER_FIRST_NAME = "TEST";

    @NotNull
    private static final String USER_MIDDLE_NAME = "TEST";

    @NotNull
    private static final String USER_LAST_NAME = "TEST";

    private static final long REPOSITORY_SIZE = 100L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @Nullable
    private static final User NULL_USER = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String EMPTY_USER_ID = null;

    @Nullable
    private static final String NULL_USER_EMAIL = null;

    @Nullable
    private static final String NULL_USER_LOGIN = null;

    @Nullable
    private static final Role NULL_USER_ROLE = null;

    @Nullable
    private static final String NULL_USER_PASSWORD = null;

    @Nullable
    private static final String EMPTY_USER_LOGIN = "";

    @Nullable
    private static final String EMPTY_USER_PASSWORD = "";

    @Nullable
    private static final String EMPTY_USER_EMAIL = "";

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "PROJECT_TASK_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "PROJECT_TASK_DESCRIPTION";

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService service = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @NotNull
    private User user;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private Project project;

    @Before
    public void setup(){
        user = createUser();
        tasks = createManyTasks();
        project = createOneProject();
    }

    @After
    public void cleanup(){
        taskRepository.clear();
        projectRepository.clear();
        userRepository.clear();
    }

    @Test
    public void create(){
        userRepository.add(user);
        Assert.assertThrows(
                LoginEmptyException.class,
                ()->service.create(EMPTY_USER_LOGIN,USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                ()->service.create(NULL_USER_LOGIN,USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                ()->service.create(USER_LOGIN_PREFIX+"TEST",EMPTY_USER_PASSWORD)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                ()->service.create(USER_LOGIN_PREFIX+"TEST",NULL_USER_PASSWORD)
        );
        Assert.assertThrows(
                LoginAlreadyExistException.class,
                ()->service.create(USER_LOGIN_PREFIX,USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                LoginAlreadyExistException.class,
                ()->service.create(USER_LOGIN_PREFIX,USER_PASSWORD_PREFIX,USER_EMAIL_PREFIX+"TEST")
        );
        Assert.assertThrows(
                RoleIsEmptyException.class,
                ()->service.create(USER_LOGIN_PREFIX+"TEST",USER_PASSWORD_PREFIX,EMPTY_USER_EMAIL+"TEST",NULL_USER_ROLE)
        );
        Assert.assertThrows(
                EmailEmptyException.class,
                ()->service.create(USER_LOGIN_PREFIX+"TEST",USER_PASSWORD_PREFIX, EMPTY_USER_EMAIL)
        );
        Assert.assertThrows(
                EmailEmptyException.class,
                ()->service.create(USER_LOGIN_PREFIX+"TEST",USER_PASSWORD_PREFIX, NULL_USER_EMAIL)
        );
        Assert.assertThrows(
                EmailAlreadyExistException.class,
                ()->service.create(USER_LOGIN_PREFIX+"TEST",USER_PASSWORD_PREFIX, USER_EMAIL_PREFIX)
        );
        service.create(USER_LOGIN_PREFIX+"TEST",USER_PASSWORD_PREFIX+"TEST");
        service.create(USER_LOGIN_PREFIX+"TEST1",USER_PASSWORD_PREFIX+"TEST",Role.USUAL);
        service.create(
                USER_LOGIN_PREFIX+"TEST2",
                USER_PASSWORD_PREFIX+"TEST",
                USER_EMAIL_PREFIX+"TEST2"
        );
        service.create(
                USER_LOGIN_PREFIX+"TEST3",
                USER_PASSWORD_PREFIX+"TEST",
                USER_EMAIL_PREFIX+"TEST3",
                Role.USUAL
        );
        Assert.assertEquals(5, userRepository.getSize());
    }

    @Test
    public void findByLogin(){
        userRepository.add(user);
        Assert.assertThrows(LoginEmptyException.class,()->service.findByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class,()->service.findByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(UserNotFoundException.class,()->service.findByLogin(UUID.randomUUID().toString()));
        Assert.assertEquals(user, service.findByLogin(USER_LOGIN_PREFIX));
    }

    @Test
    public void findByEmail(){
        userRepository.add(user);
        Assert.assertThrows(EmailEmptyException.class,()->service.findByEmail(EMPTY_USER_EMAIL));
        Assert.assertThrows(EmailEmptyException.class,()->service.findByEmail(NULL_USER_EMAIL));
        Assert.assertThrows(UserNotFoundException.class,()->service.findByEmail(UUID.randomUUID().toString()));
        Assert.assertEquals(user, service.findByEmail(USER_EMAIL_PREFIX));
    }

    @Test
    public void setPassword(){
        userRepository.add(user);
        Assert.assertThrows(UserIdEmptyException.class,()->service.setPassword(NULL_USER_ID, USER_PASSWORD_PREFIX));
        Assert.assertThrows(UserIdEmptyException.class,()->service.setPassword(EMPTY_USER_ID, USER_PASSWORD_PREFIX));
        Assert.assertThrows(PasswordEmptyException.class,()->service.setPassword(userId, EMPTY_USER_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class,()->service.setPassword(userId, NULL_USER_PASSWORD));
        @Nullable final String oldPassword = user.getPasswordHash();
        service.setPassword(userId, "NEW PASSWORD");
        Assert.assertNotEquals(oldPassword, user.getPasswordHash());
    }

    @Test
    public void updateUser(){
        userRepository.add(user);
        Assert.assertThrows(
                UserIdEmptyException.class,
                ()->service.updateUser(NULL_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                ()->service.updateUser(EMPTY_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME)
        );
        service.updateUser(userId, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME);
        Assert.assertEquals(user.getFirstName(),USER_FIRST_NAME);
        Assert.assertEquals(user.getMiddleName(),USER_MIDDLE_NAME);
        Assert.assertEquals(user.getLastName(),USER_LAST_NAME);
    }


    @Test
    public void remove(){
        taskRepository.add(tasks);
        projectRepository.add(project);
        service.add(user);
        Assert.assertThrows(ModelNotFoundException.class, ()->service.remove(NULL_USER));
        Assert.assertEquals(REPOSITORY_SIZE, taskRepository.getSize(userId));
        Assert.assertEquals(1, projectRepository.getSize(userId));
        Assert.assertEquals(1, service.getSize());
        service.remove(user);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskRepository.getSize(userId));
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectRepository.getSize(userId));
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, service.getSize());
    }

    @Test
    public void removeByLogin(){
        userRepository.add(user);
        Assert.assertThrows(LoginEmptyException.class,()->service.removeByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class,()->service.removeByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(UserNotFoundException.class,()->service.removeByLogin(UUID.randomUUID().toString()));
        Assert.assertEquals(user, service.removeByLogin(USER_LOGIN_PREFIX));
    }

    @Test
    public void isLoginExist(){
        userRepository.add(user);
        Assert.assertFalse(service.isLoginExist(EMPTY_USER_LOGIN));
        Assert.assertFalse(service.isLoginExist(NULL_USER_LOGIN));
        Assert.assertFalse(service.isLoginExist(UUID.randomUUID().toString()));
        Assert.assertTrue(service.isLoginExist(USER_LOGIN_PREFIX));
    }

    @Test
    public void isEmailExist(){
        userRepository.add(user);
        Assert.assertFalse(service.isEmailExist(EMPTY_USER_EMAIL));
        Assert.assertFalse(service.isEmailExist(NULL_USER_EMAIL));
        Assert.assertFalse(service.isEmailExist(UUID.randomUUID().toString()));
        Assert.assertTrue(service.isEmailExist(USER_EMAIL_PREFIX));
    }

    @Test
    public void lockUserByLogin(){
        userRepository.add(user);
        Assert.assertThrows(LoginEmptyException.class,()->service.lockUserByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class,()->service.lockUserByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(UserNotFoundException.class,()->service.lockUserByLogin(UUID.randomUUID().toString()));
        service.lockUserByLogin(USER_LOGIN_PREFIX);
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void unlockUserByLogin(){
        userRepository.add(user);
        Assert.assertThrows(LoginEmptyException.class,()->service.unlockUserByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class,()->service.unlockUserByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(UserNotFoundException.class,()->service.unlockUserByLogin(UUID.randomUUID().toString()));
        service.lockUserByLogin(USER_LOGIN_PREFIX);
        Assert.assertTrue(user.getLocked());
        service.unlockUserByLogin(USER_LOGIN_PREFIX);
        Assert.assertFalse(user.getLocked());
    }

    @NotNull
    private User createUser() {
        @NotNull final User user = new User();
        user.setId(userId);
        user.setLogin(USER_LOGIN_PREFIX);
        user.setPasswordHash(HashUtil.salt(USER_PASSWORD_PREFIX, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        return user;
    }

    @NotNull
    private List<Task> createManyTasks() {
        @NotNull final List<Task> tasks = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Task task = new Task();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUserId(userId);
            task.setProjectId(projectId);
            tasks.add(task);
        }
        return tasks;
    }
    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

}
