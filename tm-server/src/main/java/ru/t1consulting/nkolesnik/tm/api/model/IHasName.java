package ru.t1consulting.nkolesnik.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @Nullable
    String getName();

    void setName(@NotNull String name);

}
