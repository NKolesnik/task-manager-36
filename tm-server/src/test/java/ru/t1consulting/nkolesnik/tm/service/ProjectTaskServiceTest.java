package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectTaskService;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class ProjectTaskServiceTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private final String taskId = UUID.randomUUID().toString();

    private static final long REPOSITORY_SIZE = 1000L;

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private List<Task> tasks;

    @NotNull
    private Project project;

    @NotNull Task task;

    @Before
    public void setup(){
        task = createOneTask();
        tasks = createManyTasks();
        project = createOneProject();
    }

    @After
    public void cleanup(){
        taskRepository.clear();
        projectRepository.clear();
    }

    @Test
    public void bindTaskToProject(){
        taskRepository.add(userId, task);
        projectRepository.add(userId, project);
        Assert.assertThrows(UserIdEmptyException.class, ()->service.bindTaskToProject(NULL_USER_ID, taskId, projectId));
        Assert.assertThrows(TaskIdEmptyException.class, ()->service.bindTaskToProject(userId, projectId, NULL_TASK_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, ()->service.bindTaskToProject(userId, NULL_PROJECT_ID, taskId));
        service.bindTaskToProject(userId, projectId, taskId);
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    private Task createOneTask() {
        @NotNull final Task task = new Task();
        task.setId(taskId);
        task.setUserId(userId);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @NotNull
    private List<Task> createManyTasks() {
        @NotNull final List<Task> tasks = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Task task = new Task();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUserId(userId);
            task.setProjectId(projectId);
            tasks.add(task);
        }
        return tasks;
    }

}
