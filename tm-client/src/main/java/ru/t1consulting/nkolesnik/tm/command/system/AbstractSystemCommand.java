package ru.t1consulting.nkolesnik.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.service.ICommandService;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
