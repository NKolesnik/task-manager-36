package ru.t1consulting.nkolesnik.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataXmlFasterXmlSaveRequest extends AbstractUserRequest {

    public DataXmlFasterXmlSaveRequest(@Nullable String token) {
        super(token);
    }

}
