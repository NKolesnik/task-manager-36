package ru.t1consulting.nkolesnik.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class UserRepositoryTest {

    @NotNull
    private static final String USER_LOGIN_PREFIX = "TEST_USER_LOGIN";

    @NotNull
    private static final String USER_EMAIL_PREFIX = "TEST_USER_@EMAIL";

    @NotNull
    private static final String USER_PASSWORD_PREFIX = "TEST_USER_PASSWORD";

    @NotNull
    private static final String USER_PASSWORD_SECRET = "123654789";

    @NotNull
    private static final Integer USER_PASSWORD_ITERATION = 3;

    private static final long REPOSITORY_SIZE = 100L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String EMPTY_USER_LOGIN = "";

    @Nullable
    private static final String EMPTY_USER_EMAIL = "";

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private List<User> users;

    @NotNull
    private User user;

    @Before
    public void setup() {
        user = createUser();
        users = createManyUsers();
    }

    @After
    public void cleanup() {
        repository.clear();
    }

    @Test
    public void add() {

        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.add(user);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void addAll() {
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
        repository.add(users);
        Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void set() {
        repository.set(users);
        Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void existById() {
        repository.add(users);
        @NotNull final String id = repository.findByIndex(5).getId();
        Assert.assertTrue(repository.existsById(id));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void findAll() {
        Assert.assertNotNull(repository.findAll());
        repository.add(users);
        final long repositorySize = repository.getSize();
        final long findSize = repository.findAll().size();
        Assert.assertEquals(repositorySize, findSize);
    }

    @Test
    public void findByIndex() {
        repository.add(user);
        Assert.assertEquals(user, repository.findByIndex(0));
        Assert.assertNull(repository.findByIndex(1));
        Assert.assertNull(repository.findByIndex(-1));
    }

    @Test
    public void findById() {
        repository.add(user);
        Assert.assertEquals(user, repository.findById(userId));
        Assert.assertNull(repository.findById(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findById(NULL_USER_ID));
    }

    @Test
    public void remove() {
        repository.add(user);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNotNull(repository.remove(user));
        Assert.assertNull(repository.findById(userId));
    }

    @Test
    public void removeById() {
        repository.add(user);
        Assert.assertEquals(1, repository.getSize());
        repository.removeById(userId);
        Assert.assertNull(repository.findById(userId));
    }

    @Test
    public void removeByIndex() {
        repository.add(user);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNotNull(repository.removeByIndex(0));
        Assert.assertNull(repository.removeByIndex(1));
        Assert.assertNull(repository.findById(userId));
    }

    @Test
    public void removeAll() {
        repository.add(users);
        Assert.assertEquals(users.size(), repository.getSize());
        repository.removeAll(users);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void clear() {
        repository.add(users);
        Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
        repository.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void getSize() {
        repository.add(users);
        Assert.assertEquals(REPOSITORY_SIZE, repository.getSize());
    }

    @Test
    public void findByLogin() {
        repository.add(user);
        repository.add(users);
        Assert.assertEquals(user, repository.findByLogin(USER_LOGIN_PREFIX));
        Assert.assertNotEquals(user, repository.findByLogin(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findByLogin(EMPTY_USER_LOGIN));
    }

    @Test
    public void findByEmail() {
        repository.add(user);
        repository.add(users);
        Assert.assertEquals(user, repository.findByEmail(USER_EMAIL_PREFIX));
        Assert.assertNotEquals(user, repository.findByEmail(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findByEmail(EMPTY_USER_EMAIL));
    }

    @Test
    public void existByLogin() {
        repository.add(user);
        repository.add(users);
        Assert.assertTrue(repository.isLoginExist(USER_LOGIN_PREFIX));
        Assert.assertFalse(repository.isLoginExist(UUID.randomUUID().toString()));
    }

    @Test
    public void existByEmail() {
        repository.add(user);
        repository.add(users);
        Assert.assertTrue(repository.isEmailExist(USER_EMAIL_PREFIX));
        Assert.assertFalse(repository.isEmailExist(UUID.randomUUID().toString()));
    }

    @NotNull
    private List<User> createManyUsers() {
        @NotNull final List<User> users = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            @NotNull final User user = createUser(USER_LOGIN_PREFIX + i, HashUtil.salt(
                    USER_PASSWORD_PREFIX + i,
                    USER_PASSWORD_SECRET + i,
                    USER_PASSWORD_ITERATION+i)
            );
            user.setEmail(USER_EMAIL_PREFIX + i);
            users.add(user);
        }
        return users;
    }

    @NotNull
    private User createUser() {
        @NotNull final User user = new User();
        user.setId(userId);
        user.setLogin(USER_LOGIN_PREFIX);
        user.setPasswordHash(HashUtil.salt(USER_PASSWORD_PREFIX, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        return user;
    }

    @NotNull
    private User createUser(@NotNull final String name, @NotNull final String password) {
        @NotNull final User user = new User();
        user.setId(userId);
        user.setLogin(USER_LOGIN_PREFIX);
        user.setPasswordHash(HashUtil.salt(USER_PASSWORD_PREFIX, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        return user;
    }

}
