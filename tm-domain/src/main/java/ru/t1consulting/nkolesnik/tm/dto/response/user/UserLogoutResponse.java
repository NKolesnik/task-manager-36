package ru.t1consulting.nkolesnik.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResultResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractUserResponse;

@NoArgsConstructor
public class UserLogoutResponse extends AbstractResultResponse {

}
